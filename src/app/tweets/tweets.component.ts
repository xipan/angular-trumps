import { Component, OnInit } from '@angular/core';
import { Tweet } from '../tweet';
import { TweetsService } from '../tweets.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-tweets',
  templateUrl: './tweets.component.html',
  styleUrls: ['./tweets.component.css']
})
export class TweetsComponent implements OnInit {

  twat: Tweet[];
  twat_temp: any;
  searchword: string = 'abc';
  searchdt: string = '2019';

  constructor(private tService: TweetsService, private sanitizer: DomSanitizer) {
    this.twat = tService.tweets;
    this.twat_temp = JSON.parse(JSON.stringify(this.twat));
   }

  ngOnInit() {
  }

  onSearch(): void {
    let search = this.searchword.toString();
    let repstring;
    let twat_item_dt: Date;

    let testa: string[] = ['a', 'b', 'c', 'd'];
    let testb: string[];
    testb = testa;
    testa[0] = 'aa';
    console.log('testa:');
    console.log(testa);   
    console.log('testb:');
    console.log(testb);

    this.twat = JSON.parse(JSON.stringify(this.twat_temp));
    this.twat.forEach(function (value) {
      repstring = value['text'].toString().replace(search, '<span style=\"color:red\">' + search + '<\/span>');
      repstring = this.getSecurityHtml(repstring);
      value['text'] = repstring;
      twat_item_dt = new Date(value['date']);
      console.log(twat_item_dt.getFullYear());
    },this);
  }

  getSecurityHtml(value:string){
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

  onSearchDT(): void{
    console.log(this.searchdt.toString());
    let twat_item_dt: Date;
    this.twat = this.twat.filter(function (item) {
      twat_item_dt = new Date(item.date);
      return twat_item_dt.getFullYear() == this.searchdt.toString();
    },this);

  }

}
